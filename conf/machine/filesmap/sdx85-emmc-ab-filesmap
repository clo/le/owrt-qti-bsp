# Copyright (c) 2021, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
# Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# filename              partition

NON-HLOS.bin            /dev/block/bootdevice/by-name/modem
tools.fv                /dev/block/bootdevice/by-name/toolsfv
quantumsdk.fv           /dev/block/bootdevice/by-name/quantumsdk
Quantum.fv              /dev/block/bootdevice/by-name/quantumfv
tz.mbn                  /dev/block/bootdevice/by-name/tz
devcfg.mbn              /dev/block/bootdevice/by-name/tz_devcfg
apdp.mbn                /dev/block/bootdevice/by-name/apdp
xbl_config.elf          /dev/block/bootdevice/by-name/xbl_config
xbl_ramdump.elf         /dev/block/bootdevice/by-name/xbl_ramdump
multi_image.mbn         /dev/block/bootdevice/by-name/multi_oem
multi_image_qti.mbn     /dev/block/bootdevice/by-name/multi_qti
aop.mbn                 /dev/block/bootdevice/by-name/aop
hypvm.mbn               /dev/block/bootdevice/by-name/qhee
abl.elf                 /dev/block/bootdevice/by-name/abl
uefi.elf                /dev/block/bootdevice/by-name/uefi
sec.elf                 /dev/block/bootdevice/by-name/sec
ipa_fws.elf             /dev/block/bootdevice/by-name/ipa_fw
logfs_ufs_8mb.bin       /dev/block/bootdevice/by-name/logfs
aop_devcfg.mbn          /dev/block/bootdevice/by-name/aop_devcfg
cmnlib64.mbn            /dev/block/bootdevice/by-name/cmnlib64
qdsp6sw.mbn             /dev/block/bootdevice/by-name/core_nhlos
cpucp.elf               /dev/block/bootdevice/by-name/cpucp
fw_ipa_gsi_6.0_p.elf    /dev/block/bootdevice/by-name/ipa_fw
km41.mbn                /dev/block/bootdevice/by-name/keymaster
qupv3fw.elf             /dev/block/bootdevice/by-name/qupfw
shrm.elf                /dev/block/bootdevice/by-name/shrm
xbl_s.melf              /dev/block/bootdevice/by-name/xbl
