#! /bin/sh

#Copyright (c) 2021-2022 Qualcomm Innovation Center, Inc. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#
#   * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#
#   * Neither the name of Qualcomm Innovation Center, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

FILES=/sys/class/block/

create_symlinks()
{
        for file in $FILES/$1*
        do
                blockname=`basename $file`
                if [  $1 == "mtd" ]; then
                        partition_name=`cat $file/device/name`
                else
                        partition_name=`cat $file/uevent | awk '{ for ( n=1; n<=NF; n++ ) if($n ~ "PARTNAME") print $n }' | awk '{split($0,a, "=");print a[2]}'`
                fi
                mkdir -p /dev/block/bootdevice/by-name/
                partition_name=/dev/block/bootdevice/by-name/$partition_name
                target_dev=/dev/$blockname
                ln -s $target_dev $partition_name
        done
}


if [ ! -d /firmware/image ]; then
        if [ -f /proc/mtd ] && [ `cat /proc/mtd | wc -l` -ge "2" ]; then
                mtd_block_number=`cat /proc/mtd | grep -i modem | sed 's/^mtd//' | awk -F ':' '{print $1}'`
                echo "MTD : Detected block device : firmware for modem "
                mkdir -p $dir

                ubiattach -m $mtd_block_number -d 1 /dev/ubi_ctrl
                device=/dev/ubi1_0
                while [ 1 ]
                do
                    if [ -c $device ]
					then
                        mount -t ubifs /dev/ubi1_0 /firmware  -o bulk_read,ro,context=u:r:qcfirmware.miscfile
                        break
					else
                        sleep 1
                    fi
                done
                create_symlinks mtd
        else
		mount /dev/mmcblk0p1 /firmware -o ro,context=u:r:qcfirmware.miscfile
                create_symlinks mmc
        fi
fi

echo -n "/firmware/image" > /sys/module/firmware_class/parameters/path

if [ -f /proc/mtd ] && [ `cat /proc/mtd | wc -l` -ge "2" ]; then
	if [ ! -d /persist ]; then
		echo "creating /persist"
		mkdir -p /persist
	fi
	mount -t ubifs ubi0:persist /persist -o bulk_read
	echo "persist is mounted to /persist"
fi

#Mount cache
if [ ! -d /cache ]; then
        echo "creating /cache dir"
        mkdir -p /cache
fi

soc_id=`cat /sys/devices/soc0/soc_id`
if [ -f /proc/mtd ] && [ `cat /proc/mtd | wc -l` -ge "2" ]; then
        mount -t ubifs ubi0:cachefs /cache -o bulk_read,context=u:r:cache.miscfile
        if [ $soc_id != "570" ] && [ $soc_id != "571" ]; then
            mount -t ubifs ubi0:systemrw /overlay -o bulk_read
        fi
        mount -t ubifs /dev/ubi0_1 /data -o bulk_read,rw
else
        mount -t ext4 /dev/block/bootdevice/by-name/cache /cache -o noatime,data=ordered,noauto_da_alloc,discard,noexec,nodev,nosuid,noauto,context=u:r:cache.miscfile
        mount -t ext4 /dev/block/bootdevice/by-name/systemrw /overlay -o noatime,data=ordered,noauto_da_alloc,discard,noexec,nodev,nosuid,noauto
        mount -t ext4 /dev/block/bootdevice/by-name/userdata /data
        mount -t ext4 /dev/block/bootdevice/by-name/persist /persist -o noatime,data=ordered,noauto_da_alloc,discard,noexec,nodev,nosuid,noauto,context=u:r:persist.miscfile
fi

if [ $soc_id == "570" ] || [ $soc_id == "571" ]; then
    mkdir -p /data/overlay-work
    mkdir -p /data/overlay-work/etc-upper
    mkdir -p /data/overlay-work/.etc-work
    chcon -t file.conffile /data/overlay-work/.etc-work
    mount -t overlay -o lowerdir=/etc,upperdir=/data/overlay-work/etc-upper,workdir=/data/overlay-work/.etc-work overlay /etc
else
    mkdir -p /overlay/etc-upper
    mkdir -p /overlay/.etc-work
    chcon -t file.conffile /overlay/.etc-work
    mount -t overlay -o lowerdir=/etc,upperdir=/overlay/etc-upper,workdir=/overlay/.etc-work overlay /etc
fi

RESTORECON=/sbin/restorecon
${RESTORECON} -R  /etc -e /etc/rc.d

# For  Boot KPI we will call restorcon only  for the first boot
# any new files will get labeled on creation

if [ ! -f /persist/.autolabeled ]; then
     # Need Restorecon for /persist
     ${RESTORECON} -RF /persist
     touch  /persist/.autolabeled
fi

if [ ! -f /data/.autolabeled ]; then
        # Need Restorecon for /data
        ${RESTORECON} -RF /data
        touch  /data/.autolabeled
fi
